"use strict";

var app = angular.module('exotica', []);
var controller = app.controller('exoticaController', function ($scope, $http, $log) {
  return $http({ method: 'get', url: '/getPrograms' }).success(function (data) {
    return $scope.programs = data;
  });
});