'use strict'
const app = angular.module('exotica', [])
app.controller('exoticaController',
  ($scope, $http, $log) =>
    $http({method: 'get', url: '/getPrograms'})
    .success((data) => $scope.programs = data)
)
