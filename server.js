'use strict'
const express = require('express')
const app = express()
const mongo = require('mongodb')
const MongoClient = mongo.MongoClient
const dbUrl = process.env.MONGOLAB_URI || 'mongodb://localhost:27017/exotica'
const loadFile = require('./helpers/load_file')
const getPrograms = require('./helpers/get_programs')

console.log('connecting to db ' + dbUrl)

MongoClient.connect(dbUrl, (err, db) => {
  if (err) {
    throw err
  }
  console.log('db connected')

  app.use(express.static('public'))

  app.get('/getPrograms', (req, res) => getPrograms(db, req, res))
  app.get('/:page?', (req, res) => res.render('index'))
  app.get('/file/:id', (req, res) => loadFile(db, req, res))
  app.set('view engine', 'pug')
  const port = process.env.PORT || 3000
  app.listen(port, () => console.log('listening on port ' + port))
}
)
