/* jshint node: true */

'use strict';
let gulp = require('gulp');
let babel = require('gulp-babel');
let sass = require('gulp-sass');
let clean = require('gulp-clean');

let config = {
  scss: './front/scss/**/*.scss',
  js: './front/**/*.js',
  destCss: './public/css',
  destJs: './public/js',
  babel: {presets: ['es2015']}
};

gulp.task('sass', ['clearCss'], () => {
  gulp.src(config.scss)
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest(config.destCss));
});

gulp.task('js', ['clearJs'], () => {
  gulp
    .src(config.js)
    .pipe(babel(config.babel))
    .pipe(gulp.dest(config.destJs));
});

gulp.task('watch', () => {
  gulp.watch(config.scss, ['sass']);
  gulp.watch(config.js, ['js']);
});

gulp.task('clearJs', () => {
  return gulp
    .src(config.destJs, {read: false})
    .pipe(clean());
});

gulp.task('clearCss', () => {
  return gulp
    .src(config.destCss, {read: false})
    .pipe(clean());
});

gulp.task('default', ['js', 'sass']);
