'use strict'
const fs = require('fs')

// let loadFile = (db, req, res) => {
//   let id = req.params.id;
//   res.writeHead(200, {
//     'Content-Type': 'audio/mpeg',
//     'Content-Length': 78872641
//   });
//   console.log("id="+id);
//   // let readStream = bucket.openDownloadStream(ObjectId(id));
//   console.log("after stream open");
//   // readStream.pipe(res);
//   let s3 = new aws.S3({ params: {Bucket: 'radioexotica'}});
//   let params = { Key: id};

//   // s3.getObject(params).createReadStream().pipe(res);
//   console.log("before get object");

//   s3.getObject(params).
//     on('httpData', function(chunk) { res.write(chunk); }).
//     // on('httpDone', function() { res.end(); }).
//     send();
// };

const loadFile1 = (db, req, res) => {
  const id = req.params.id
  const S3 = require('aws-sdk').S3
  const S3S = require('s3-streams')

  var download = S3S.ReadStream(new S3(), {
    Bucket: 'radioexotica',
    Key: id
      // Any other AWS SDK options
  })

  res.writeHead(200, {
    // 'Content-Type': 'audio/mp3',
    // 'Content-Length': 78831079
  })

  // let res1 = fs.createWriteStream('test.mp3');
  // download.pipe(res1);

  download.on('end', () => res.end())
  download.on('data', (chunk) => res.write(chunk))
}

const loadFile = (db, req, res) => {
  const fileName = 'test3.mp3'

  const file = fs.createReadStream(fileName)
  var stat = fs.statSync(fileName)
  res.writeHead(200, {
    'Content-Type': 'audio/mp3',
    'Content-Length': stat.size
  })

  console.log('file size = ' + stat.size)
  file.pipe(res)
}

module.exports = loadFile
