'use strict'

function loadDescriptions () {
  let http = require('http')
  let mongo = require('mongodb')
  let MongoClient = mongo.MongoClient
  let m = require('moment')
  m.locale('ru')
  let cheerio = require('cheerio')

  let dbUrl = process.env.MONGOLAB_URI || 'mongodb://localhost:27017/exotica'
  let descUrl = 'http://www.cultradio.ru/brand/audio/id/57112/'

  let getCallback = function (res, db) {
    let html = ''
    res.on('data', chunk => html += chunk)
    res.on('end', function () {
      let $ = cheerio.load(html)
      let blocks = $('.audio-block')
      let programs = blocks.map(function () {
        let $this = $(this)
        try {
          var dateTag = $($this.find('.date a'))
          var dateText = dateTag.text()
          var date = m(dateText, 'DD MMM Y').toDate()
          var text = $this.find('.text').text().trim()
        } catch (e) {
          console.log(`cant parse block ${$this.html()}: ${e}`)
          throw e
        }

        return db.collection('programs').updateOne(
          {date},
          {date, text},
          {upsert: true}
        )
      }).toArray()

      console.log(`${programs.length} programs found`)

      Promise.all(programs).then(() => {
        db.close()
        console.log('db closed')
      })
    })
  }

  MongoClient.connect(dbUrl, function (err, db) {
    if (err) {
      throw err
    }
    console.log('Connected correctly to server ' + dbUrl)
    http.get(descUrl, (res) => getCallback(res, db))
  })
}

module.exports = loadDescriptions
