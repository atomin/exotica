/* jshint node: true */
'use strict'

function record (minutes, radio = 'culture') {
  minutes = minutes || 55
  console.log(`recording ${minutes} minute of radio ${radio}`)
  const http = require('http')
  const fs = require('fs')
  const m = require('moment')
  const urls = {
    culture: 'http://icecast.vgtrk.cdnvideo.ru/kulturafm_mp3_192kbps', // culture
    rossii: 'http://icecast.vgtrk.cdnvideo.ru/rrzonam_mp3_192kbps' // rossii
  }

  const streamUrl = urls[radio]

  const getCallback = (res) => {
    const dateStr = m(new Date()).format('Y_MM_DD_HH_mm_ss')
    const fileName = `exotica_${dateStr}.mp3`
    console.log(fileName)
    const upStream = fs.createWriteStream(fileName)
    res.pipe(upStream)
    const endRecord = () => {
      console.log('end record')
      res.destroy()
      upStream.close()
    }
    setTimeout(endRecord, minutes * 60 * 1000)
  }

  http.get(streamUrl, getCallback)
}

module.exports = record
