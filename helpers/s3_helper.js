/* jshint node: true */

'use strict'
const m = require('moment')

class S3Helper {
  getBucketList (bucketName = 'radioexotica') {
    const aws = require('aws-sdk')
    const bucket = new aws.S3({params: {Bucket: bucketName}})

    const p = new Promise((resolve, reject) => {
      bucket.listObjects((err, data) => {
        if (err) reject(err)
        else resolve(this.process(data.Contents))
      })
    })

    return p
  }

  process (data) {
    return data.map(r => {
      const template = /exotica_(\d{4})_(\d{2})_(\d{2}).*/
      const dateString = r.Key.replace(template, '$1 $2 $3')
      const d = m(dateString, 'YYYY MM DD')
      d.add(1, 'day')
      r.date = d.toDate()
      console.dir(r)
      return r
    })
  }
}

module.exports = S3Helper
