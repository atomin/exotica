/* jshint node: true */
'use strict'

function recordToS3 (minutes) {
  minutes = minutes || 55
  console.log(`recording ${minutes} minute`)
  const http = require('http')
  const m = require('moment')
  const streamUrl = 'http://icecast.vgtrk.cdnvideo.ru/kulturafm_mp3_192kbps'// culture
  // let streamUrl= 'http://icecast.vgtrk.cdnvideo.ru/rrzonam_mp3_192kbps';//rossii

  const getCallback = (res) => {
    const dateStr = m(new Date()).format('Y_MM_DD_HH_mm_ss')
    const fileName = `exotica_${dateStr}.mp3`
    console.log(fileName)

    const bucketName = 'radioexotica'
    const aws = require('aws-sdk')
    const bucket = new aws.S3({ params: { Bucket: bucketName, Key: fileName } })

    bucket.upload({ Body: res })
      .on('httpUploadProgress', evt => console.log(evt))
      .send((err, data) => console.log(err, data))

    const endRecord = () => {
      console.log('end record')
      res.destroy()
    }
    res.on('end', () => {
      console.log('end')
    })
    setTimeout(endRecord, minutes * 60 * 1000)
  }

  http.get(streamUrl, getCallback)
}

module.exports = recordToS3
