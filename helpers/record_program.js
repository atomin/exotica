'use strict'
function record (mins) {
  mins = mins || 55
  console.log(`recording ${mins} minute`)
  const http = require('http')
  const mongo = require('mongodb')
  const MongoClient = mongo.MongoClient
  const m = require('moment')

  const dbUrl = process.env.MONGOLAB_URI || 'mongodb://localhost:27017/exotica'
  const streamUrl = 'http://livestream.rfn.ru:8080/kulturafm/mp3_192kbps'

  const getCallback = (res, db) => {
    const bucket = new mongo.GridFSBucket(db, {
      bucketName: 'exotica'
    })
    const dateStr = m(new Date()).format('Y_MM_DD')
    const fileName = `exotica_${dateStr}.mp3`
    console.log(fileName)
    const upStream = bucket.openUploadStream(fileName)
    res.pipe(upStream)
    const endRecord = () => {
      console.log('end record')
      res.destroy()
      upStream.end(null, null, () => {
        console.log('db close')
        db.close()
      })
    }
    setTimeout(endRecord, mins * 60 * 1000)
  }

  MongoClient.connect(dbUrl, (err, db) => {
    if (err) {
      throw err
    }
    console.log('Connected correctly to server ' + dbUrl)
    http.get(streamUrl, (res) => getCallback(res, db))
  })
}

module.exports = record
