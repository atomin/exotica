'use strict'

let getPage = (db, req, res) => {
  let page = req.params.page || 'index'
  console.log('page: ' + page)
  if (page !== 'index' && page !== 'index_old') {
    res.send(`page ${page} notfound`, 404)
    return
  }

  let programsPromise = db.collection('programs').find().toArray()
  let filesPromise = db.collection('exotica.files').find().toArray()

  Promise.all([programsPromise, filesPromise]).then(values => {
    let [programs, files] = values
    let finalPrograms = programs.slice()

    files.forEach(f => {
      f.date = new Date(f.uploadDate)
      f.date.setHours(0, 0, 0, 0)
      f.audio_id = f._id
      f.text = 'Нет описания'
      let program = programs.find(p => p.date - f.date === 0)
      if (program) {
        program.audio_id = f.audio_id
      } else {
        finalPrograms.push(f)
      }
    })

    finalPrograms.sort((x, y) => y.date - x.date)

    res.render(page, {programs: finalPrograms, m: require('moment')})
  })
}

module.exports = getPage
