'use strict'

let getPrograms = (db, req, res) => {
  let S3Helper = require('../helpers/s3_helper.js')
  let s3Helper = new S3Helper()
  let page = req.params.page || 'index'
  console.log('page: ' + page)
  if (page !== 'index') {
    res.send(`page ${page} notfound`)
    return
  }

  let programsPromise = db.collection('programs').find().toArray()
  let filesPromise = s3Helper.getBucketList()

  Promise.all([programsPromise, filesPromise]).then(([programs, files]) => {
    let finalPrograms = programs.slice()

    files.forEach(f => {
      f.audio_id = f.Key
      f.text = 'Нет описания'
      let program = programs.find(p => p.date - f.date === 0)
      if (program) {
        program.audio_id = f.audio_id
      } else {
        finalPrograms.push(f)
      }
    })

    finalPrograms.sort((x, y) => y.date - x.date)

    res.json(finalPrograms)
  })
}

module.exports = getPrograms
