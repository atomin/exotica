/*jshint node: true*/
"use strict";

let should = require('chai').should();

let chai = require("chai");
let chaiAsPromised = require("chai-as-promised");
 
chai.use(chaiAsPromised);

let S3Helper = require('../helpers/s3_helper');
let s3_helper = new S3Helper();


describe('s3_helper', function(){
  describe('#getBucketList', function() {
    this.timeout(10000);
    it('get test bucket content', () => {
      let promise = s3_helper.getBucketList('tomintestbucket');
      return promise
        .should.eventually
        .have.length(1)
        .have.deep.property('[0].Key', "testfile.txt");
    });
  });
});